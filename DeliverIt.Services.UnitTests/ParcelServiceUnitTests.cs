using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIt.Services.UnitTests
{
    [TestClass]
    public class ParcelSerivceUnitTests
    {
        [TestMethod]
        public async Task ParcelService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>();

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new CreateParcelRequestModel()
            {
                Weight = 10,
                DeliveryToAddress = false,
                CustomerId = 1,
                WarehouseId = 1,
                CategoryId = 1,
                ShipmentId = 1
            };
            var result = await service.CreateParcelAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ParcelService_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>();

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new CreateParcelRequestModel()
            {
                Weight = 10,
                DeliveryToAddress = false,
                CustomerId = 0,
                WarehouseId = -1,
                CategoryId = 1,
                ShipmentId = 1
            };
            var result = await service.CreateParcelAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ParcelServiceUpdate_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 11,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new UpdateParcelRequestModel()
            {
                Id = 1,
                Weight = 10,
                DeliveryToAddress = true,
                CustomerId = 2,
                WarehouseId = 2,
                CategoryId = 2,
                ShipmentId = 2
            };
            var result = await service.UpdateParcelAsync(request);

            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ParcelServiceUpdate_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 11,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new UpdateParcelRequestModel()
            {
                Id = 1,
                Weight = 10,
                DeliveryToAddress = true,
                CustomerId = 1,
                WarehouseId = -1,
                CategoryId = 1,
                ShipmentId = 1
            };
            var result = await service.UpdateParcelAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ParcelServiceUpdate_ShouldReturnFalse_WhenParcelNotFound()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 11,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new UpdateParcelRequestModel()
            {
                Id = 5,
                Weight = 10,
                DeliveryToAddress = true,
                CustomerId = 1,
                WarehouseId = 1,
                CategoryId = 1,
                ShipmentId = 1
            };
            var result = await service.UpdateParcelAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ParcelServiceDelete_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 11,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new DeleteParcelRequestModel()
            {
                Id = 1
            };
            var result = await service.DeleteParcelAsync(request);

            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ParcelServiceDelete_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 11,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ParcelService(mockContext.Object);

            var request = new DeleteParcelRequestModel()
            {
                Id = 5
            };
            var result = await service.DeleteParcelAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ParcelServiceFilter_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCategories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Test"
                }
            };

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 10,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockDbCategories = mockCategories.AsQueryable().BuildMockDbSet();
            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Categories).Returns(mockDbCategories.Object);

            var service = new ParcelService(mockContext.Object);

            var request = new FilterParcelRequesteModel()
            {
                Weight = 10,
                CustomerId = -1,
                WarehouseId = 1,
                CategoryName = "Test",
                OrderByWeight = false,
                OrderByArrivalDate = false,
                OrderDescending = false
            };
            var result = await service.FilterParcelAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ParcelServiceFilter_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCategories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Test"
                }
            };

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    Weight = 10,
                    DeliveryToAddress = false,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1
                }
            };

            var mockDbCategories = mockCategories.AsQueryable().BuildMockDbSet();
            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbSetParcel = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Categories).Returns(mockDbCategories.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbSetParcel.Object);

            var service = new ParcelService(mockContext.Object);

            var request = new FilterParcelRequesteModel()
            {
                Weight = 10,
                CustomerId = 1,
                WarehouseId = 1,
                CategoryName = "Test",
                OrderByWeight = true,
                OrderByArrivalDate = true,
                OrderDescending = false
            };
            var request2 = new FilterParcelRequesteModel()
            {
                Weight = 10,
                CustomerId = 1,
                WarehouseId = 1,
                CategoryName = "Test",
                OrderByWeight = true,
                OrderByArrivalDate = true,
                OrderDescending = true
            };
            var result = await service.FilterParcelAsync(request);
            var result2 = await service.FilterParcelAsync(request2);


            Assert.AreEqual(result.IsSuccess, true);
            Assert.AreEqual(result2.IsSuccess, true);
        }
    }
}