using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIt.Services.UnitTests
{
    [TestClass]
    public class EmployeeServiceUnitTests
    {
        [TestMethod]
        public async Task EmployeeService_ShouldRegister_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",                    
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new RegisterEmployeeRequestModel()
            {
               Username = "TestEmployee",
               Password = "12345",
               FirstName = "FirstName",
               LastName = "LastName",
               Email = "testMail@mailtest.com",

            };
            var result = await service.RegisterEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task EmployeeService_ReturnFalse_WhenUserExists()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new RegisterEmployeeRequestModel()
            {
                Username = "TestUser",
                Password = "12345",
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "testMail@mailtest.com"
            };
            var result = await service.RegisterEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task EmployeeService_ReturnFlase_WhenEmployeeExists()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new RegisterEmployeeRequestModel()
            {
                Username = "TestEmployee",
                Password = "12345",
                FirstName = "Test",
                LastName = "User",
                Email = "testmail@mailtest.com",

            };
            var result = await service.RegisterEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task EmployeeService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new RegisterEmployeeRequestModel()
            {
                Username = "TestEmployee",
                Password = "12345",
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "testMail@mailtest.com",
                Address = "testaddress",
                CityName = "Burgas",
                CountryName = "Bulgaria"
            };
            var result = await service.RegisterEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task EmployeeService_ShouldDelete_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new DeleteEmployeeRequestModel()
            {
                Id = 1
            };
            var result = await service.DeleteEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task EmployeeService_DeleteReturnFalse_WhenEmployeeNotFound()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new DeleteEmployeeRequestModel()
            {
                Id = 5
            };
            var result = await service.DeleteEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task EmployeeService_UpdateReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "NewAddress",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new UpdateEmployeeRequestModel()
            {
                EmployeeId = 1,
                FirstName = "TestName",
                LastName = "LastNameTest",
                Email = "newmail@testmail.com",
                AddressId = 2
            };
            var result = await service.UpdateEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task EmployeeService_UpdateReturnFalse_WhenNotFound()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "NewAddress",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new UpdateEmployeeRequestModel()
            {
                EmployeeId = 2,
                FirstName = "TestName",
                LastName = "LastNameTest",
                Email = "newmail@testmail.com",
                AddressId = 2
            };
            var result = await service.UpdateEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task EmployeeService_UpdateReturnFalse_WhenNoChange()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "NewAddress",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    UserId = 1,
                    AddressId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockEmployees = employees.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Employees).Returns(mockEmployees.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new EmployeeService(mockContext.Object);

            var request = new UpdateEmployeeRequestModel()
            {
                EmployeeId = 1,
                FirstName = "Test",
                LastName = "User",
                Email = "testmail@mailtest.com",
                AddressId = 1
            };
            var result = await service.UpdateEmployeeAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }
    }
}