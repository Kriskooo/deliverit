using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIt.Services.UnitTests
{
    [TestClass]
    public class ShipmentSerivceUnitTests
    {
        [TestMethod]
        public async Task ShipmentService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>();

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new CreateShipmentRequestModel()
            {
                OriginWarehouse = 1,
                DestinationWarehouse = 2,
                DepartureTime = DateTime.Now.AddDays(1),
                ArrivalTime = DateTime.Now.AddDays(2)
            };
            var result = await service.CreateShipmentAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ShipmentService_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>();

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new CreateShipmentRequestModel()
            {
                OriginWarehouse = 0,
                DestinationWarehouse = 2,
                DepartureTime = DateTime.Now.AddDays(-21),
                ArrivalTime = DateTime.Now.AddDays(2)
            };
            var result = await service.CreateShipmentAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceUpdate_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new UpdateShipmentRequestModel()
            {
                Id = 1,
                OriginWarehouseId = 2,
                DestinationWarehouseId = 1,
                DepartureTime = DateTime.Now.AddDays(1),
                ArrivalTime = DateTime.Now.AddDays(3),
                StatusId = 2
            };
            var result = await service.UpdateShipmentAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ShipmentServiceUpdate_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>();

            var mockWarehouses = new List<Warehouse>();
            var mockShipments = new List<Shipment>();

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new UpdateShipmentRequestModel()
            {
                Id = 1,
                OriginWarehouseId = 0,
                DestinationWarehouseId = -1,
                DepartureTime = DateTime.Now.AddDays(-5),
                ArrivalTime = DateTime.Now.AddDays(-3),
                StatusId = 2
            };
            var result = await service.UpdateShipmentAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceDelete_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new DeleteShipmentRequestModel()
            {
                Id = 1
            };
            var result = await service.DeleteShipmentAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ShipmentServiceDelete_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new DeleteShipmentRequestModel()
            {
                Id = 5
            };
            var result = await service.DeleteShipmentAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByOriginWarehouse_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByWarehouseRequestModel()
            {
                WarehouseId = 1
            };
            var result = await service.GetShipmentByOriginWarehouseAsync(request);
            Assert.AreEqual(result.TotalCount, 1);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByOriginWarehouse_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByWarehouseRequestModel()
            {
                WarehouseId = -1
            };
            var result = await service.GetShipmentByOriginWarehouseAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByOriginWarehouse_ShouldReturnFalse_WhenNoSuchShipment()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByWarehouseRequestModel()
            {
                WarehouseId = 3
            };
            var result = await service.GetShipmentByOriginWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByDestinationWarehouse_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByWarehouseRequestModel()
            {
                WarehouseId = 2
            };
            var result = await service.GetShipmentByDestinationWarehouseAsync(request);
            Assert.AreEqual(result.TotalCount, 1);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByDestinationWarehouse_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByWarehouseRequestModel()
            {
                WarehouseId = -1
            };
            var result = await service.GetShipmentByDestinationWarehouseAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByDestinationWarehouse_ShouldReturnFalse_WhenNoSuchShipment()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByWarehouseRequestModel()
            {
                WarehouseId = 3
            };
            var result = await service.GetShipmentByDestinationWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByCustomer_ShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };
            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                     Id = 1,
                     CustomerId = 1,
                     CategoryId = 1,
                     DeliveryToAddress = false,
                     ShipmentId = 1,
                     WarehouseId = 2,
                     Weight = 10
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByCustomerRequestModel()
            {
                CustomerId = 1
            };
            var result = await service.GetShipmentByCustomerAsync(request);
            Assert.AreEqual(result.TotalCount, 1);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByCustomer_ShouldReturnFalse_WhenParametersNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);


            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByCustomerRequestModel()
            {
                CustomerId = -1
            };
            var result = await service.GetShipmentByCustomerAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task ShipmentServiceGetByCustomer_ShouldReturnFalse_WhenNoSuchShipment()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockParcels = new List<Parcel>
            {
                new Parcel
                {
                     Id = 1,
                     CustomerId = 1,
                     CategoryId = 1,
                     DeliveryToAddress = false,
                     ShipmentId = 1,
                     WarehouseId = 2,
                     Weight = 10
                }
            };

            var mockShipmentStatuses = new List<ShipmentStatus>
            {
                new ShipmentStatus
                {
                    Id = 1,
                    Name = "Preparing"
                },
                new ShipmentStatus
                {
                    Id = 2,
                    Name = "On the way"
                },
                new ShipmentStatus
                {
                    Id = 3,
                    Name = "Delivered"
                }
            };

            var mockWarehouses = new List<Warehouse>
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };

            var mockShipments = new List<Shipment>
            {
                new Shipment
                {
                   Id = 1,
                   OriginWarehouseId = 1,
                   DestinationWarehouseId = 2,
                   DepartureDate = DateTime.Now.AddDays(-1),
                   ArrivalTime = DateTime.Now.AddDays(2),
                   StatusId = 1
                }
            };

            var mockDbSet = mockShipmentStatuses.AsQueryable().BuildMockDbSet();
            var mockDbWarehouses = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockDbShipments = mockShipments.AsQueryable().BuildMockDbSet();
            var mockDbParcels = mockParcels.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.ShipmentStatuses).Returns(mockDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockDbWarehouses.Object);
            mockContext.Setup(db => db.Shipments).Returns(mockDbShipments.Object);
            mockContext.Setup(db => db.Parcels).Returns(mockDbParcels.Object);

            var service = new ShipmentService(mockContext.Object);

            var request = new GetShipmentsByCustomerRequestModel()
            {
                CustomerId = 3
            };
            var result = await service.GetShipmentByCustomerAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
        }
    }
}


