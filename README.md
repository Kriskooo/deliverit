- DeliverIT's customers can place orders on international shopping sites that don’t provide delivery to their location (like Amazon.de or eBay.com) and have their parcels delivered either to the company's warehouses or their own address.

- DeliverIT has two types of users - customers and employees. The customers can trach the status of their parcels. The employees have a bit more capabilities. They can add new parcels to the system group them in shipments and track their location and status.
