﻿using DeliverIT.Models.EntityModels;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Data
{
    public interface IDeliverITContext
    {
        DbSet<Address> Addresses { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<Parcel> Parcels { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<Shipment> Shipments { get; set; }
        DbSet<ShipmentStatus> ShipmentStatuses { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Warehouse> Warehouses { get; set; }
    }
}