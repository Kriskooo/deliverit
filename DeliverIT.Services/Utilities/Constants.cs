﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Utilities
{
    public class Constants
    {
        public const string USER_CUSTOMER_ALREADY_EXISTS = "User or Customer Already Exists";
        public const string COUNTRY_NOT_SUPPORTED = "Country not supported";
        public const string CITY_NOT_SUPPORTED = "City not supported";
        public const string CUSTOMER_REGISTERED = "You have been registered!";
        public const string PASSWORD_IS_INCORRECT = "Password is incorrect";
        public const string USER_LOGGED = "You have logged";
        public const string NO_SUCH_USER = "No such user!";
        public const string PARCEL_WRONG_PARAMETERS = "Wrong parameters";
        public const string PARCEL_CREATED = "Parcel Created";
        public const string SHIPMENT_WRONG_PARAMETERS = "Wrong parameters";
        public const string SHIPMENT_CREATED = "Shipment Created";
        public const string WAREHOUSE_ID_NOT_EXISTS = "Warehouse Id you selected does not exist";
        public const string NO_SUCH_SHIPMENT = "No such shipment";
        public const string SUCCESS_CREATED_WAREHOUSE = "Success created warehouse";
        public const string ADDRESS_ID_NOT_EXISTS = "AddressId is not exist";
        public const string WAREHOUSE_NOT_FOUND = "Warehouse not found";
        public const string WAREHOUSE_DELETED = "Warehouse successfully is deleted";
        public const string WAREHOUSE_CHANGED = "Warehouse was changed";
        public const string WAREHOUSE_FOUND = "Warehouse was found but there was no changes";
        public const string WAREHOUSE_WRONG_PARAMETERS = "Wrong Parameters";
        public const string CUSTOMER_NOT_FOUND = "Customer not found";
        public const string CUSTOMER_DELETED = "Customer succsessfully is deleted";
        public const string CUSTOMER_CHANGED = "Customer was changed";
        public const string CUSTOMER_FOUND = "Customer was found but there was no changes";


        public const int PAGINATION_SIZE = 10;

        public const string EMAIL_REGEX = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$";

        public const string AdminRoleName = "Admin";
        public const string EmployeeRoleName = "Employee";
        public const string CustomerRoleName = "Customer";
    }
}
