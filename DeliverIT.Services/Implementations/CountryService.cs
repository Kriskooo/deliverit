﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Implementations
{
    public class CountryService : ICountryService
    {
        private readonly DeliverITContext db;

        public CountryService(DeliverITContext db)
        {
            this.db = db;
        }

        public async Task<GetCountryResponseModel> GetCountries()
        {
            var result = new GetCountryResponseModel();
            var listOfCountries = await db.Countries.Where(p => p.Id > 0).Select(p => p.Name).ToListAsync();
            result.Countries = listOfCountries;
            return result;
        }
    }
}
