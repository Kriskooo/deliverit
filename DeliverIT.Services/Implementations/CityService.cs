﻿using DeliverIT.Data;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Implementations
{
    public class CityService : ICityService
    {
        private readonly DeliverITContext db;

        public CityService(DeliverITContext db)
        {
            this.db = db;
        }

        public async Task<GetCityResponseModel> GetCities()
        {
            var result = new GetCityResponseModel();
            var listOfCities = await db.Cities.Where(p => p.Id > 0).Select(p => p.Name).ToListAsync();
            result.Cities = listOfCities;
            return result;
        }
    }
}
