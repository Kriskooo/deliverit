﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Services.Implementations
{
    public class WarehouseService : IWarehouseService
    {
        private readonly DeliverITContext db;

        public WarehouseService(DeliverITContext db)
        {
            this.db = db;
        }

        public async Task<GetWarehouseResponseModel> GetWarehouseByCityAsync(GetWarehouseRequestModelByCity requestModel)
        {
            var ResponseModel = new GetWarehouseResponseModel();
            var warehouses = await db.Warehouses
                .Include(w => w.Address)
                .ThenInclude(a => a.City)
                .Where(w => w.Address.City.Name == requestModel.CityName)
                .ToListAsync();
            ResponseModel.Warehouses = warehouses;
            return ResponseModel;
        }

        public async Task<GetWarehouseResponseModel> GetWarehouseByCountryAsync(GetWarehouseRequestModelByCountry requestModel)
        {
            var ResponseModel = new GetWarehouseResponseModel();
            var warehouses = await db.Warehouses.Include(w => w.Address).ThenInclude(a => a.City).ThenInclude(c => c.Country)
                .Where(w => w.Address.City.Country.Name == requestModel.CountryName).ToListAsync();
            ResponseModel.Warehouses = warehouses;
            return ResponseModel;
        }

        public async Task<CreateWarehouseResponseModel> CreateWarehouseAsync(CreateWarehouseRequestModel requestModel)
        {
            var responseModel = new CreateWarehouseResponseModel();
            if (requestModel.AddressId < 1)
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.WAREHOUSE_WRONG_PARAMETERS;
            }

            if (await this.db.Addresses.AnyAsync(a => a.Id == requestModel.AddressId))
            {
                var warehouses = new Warehouse()
                {
                    AddressId = requestModel.AddressId
                };
                this.db.Warehouses.Add(warehouses);
                await this.db.SaveChangesAsync();
                responseModel.IsSuccess = true;
                responseModel.Message = Constants.SUCCESS_CREATED_WAREHOUSE;
            }

            else
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.ADDRESS_ID_NOT_EXISTS;
            }
            return responseModel;
        }

        public async Task<DeleteWarehouseResponseModel> DeleteWarehouseAsync(DeleteWarehouseRequestModel requestModel)
        {
            var responseModel = new DeleteWarehouseResponseModel();
            var warehouse = await this.db.Warehouses.FirstOrDefaultAsync(w => w.Id == requestModel.Id);

            if (warehouse == null)
            {
                responseModel.Message = Constants.WAREHOUSE_NOT_FOUND;
                responseModel.IsSuccess = false;
            }

            else
            {
                responseModel.Message = Constants.WAREHOUSE_DELETED;
                responseModel.IsSuccess = true;
                this.db.Warehouses.Remove(warehouse);
                await this.db.SaveChangesAsync();

            }
            return responseModel;
        }

        public async Task<UpdateWarehousesResponseModel> UpdateWarehouseAsync(UpdateWarehouseRequestModel requestModel)
        {
            var responseModel = new UpdateWarehousesResponseModel();
            if (requestModel.AddressId < 1)
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.WAREHOUSE_WRONG_PARAMETERS;
            }

            var warehouse = await this.db.Warehouses.FirstOrDefaultAsync(w => w.Id == requestModel.Id);

            if (warehouse == null)
            {
                responseModel.Message = Constants.WAREHOUSE_NOT_FOUND;
                responseModel.IsSuccess = false;
                return responseModel;
            }

            if (warehouse.AddressId != requestModel.AddressId)
            {
                warehouse.AddressId = requestModel.AddressId;
                responseModel.IsSuccess = true;
                responseModel.Message = Constants.WAREHOUSE_CHANGED;

                this.db.Warehouses.Update(warehouse);
                await this.db.SaveChangesAsync();
            }
            else
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.WAREHOUSE_FOUND;
            }

            return responseModel;
        }

        public async Task<TotalWarehousesResponseModel> TotalWarehousesAsync()
        {
            var responseModel = new TotalWarehousesResponseModel();
            responseModel.Warehouses = await db.Warehouses.CountAsync();
            return responseModel;
        }
    }
}