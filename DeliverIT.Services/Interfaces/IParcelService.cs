﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface IParcelService
    {
        Task<CreateParcelResponseModel> CreateParcelAsync(CreateParcelRequestModel requestModel);
        Task<UpdateParcelResponseModel> UpdateParcelAsync(UpdateParcelRequestModel requestModel);
        Task<DeleteParcelResponseModel> DeleteParcelAsync(DeleteParcelRequestModel requestModel);
        Task<FilterParcelResponseModel> FilterParcelAsync(FilterParcelRequesteModel requestModel);
        Task<GetParcelsResponse> GetParcelAsync(GetParcelsRequestModel requestModel);
        Task<GetParcelResponseModel> GetParcelByIdAsync(EditParcelRequestModel requestModel);
    }
}
