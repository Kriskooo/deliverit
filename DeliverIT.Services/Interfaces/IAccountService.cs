﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface IAccountService
    {
        Task<GetLoggedUserInfoResponseModel> GetLoggedUserInfoAsync(GetLoggedUserInfoRequestModel requestModel);
        Task<RegisterUserResponseModel> RegisterUser(RegisterUserRequestModel requestModel);
    }
}
