﻿using DeliverIT.Models.ResponseModels;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface ICountryService
    {
        Task<GetCountryResponseModel> GetCountries();
    }
}
