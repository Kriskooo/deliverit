﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface ICustomerService
    {
        Task<UpdateCustomerResponseModel> UpdateCustomerAsync(UpdateCustomerRequestModel requestModel);
        Task<DeleteCustomerResponseModel> DeleteCustomerAsync(DeleteCustomerRequestModel requestModel);
        Task<TotalCustomersResponseModel> TotalCustomersAsync();
    }
}
