﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeliverIT.Models.EntityModels
{
    public class Warehouse
    {
        [Key]
        public int Id { get; set; }        
        [Required]
        [ForeignKey("Address")]
        public long AddressId { get; set; }
        public Address Address { get; set; }
       
        public virtual ICollection<Parcel> Parcels { get; set; }
        public virtual ICollection<Shipment> OriginShipments { get; set; }
        public virtual ICollection<Shipment> DestinationShipments { get; set; }
    }
}
