﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeliverIT.Models.EntityModels
{
    public class Customer
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [ForeignKey("Address")]
        public long AddressId { get; set; }
        public Address Address { get; set; }
        [Required]
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual ICollection<Parcel> Parcels { get; set; }
    }
}
