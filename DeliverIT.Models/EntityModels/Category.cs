﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Models.EntityModels
{
    public class Category
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Parcel> Parcels { get; set; }
    }
}
