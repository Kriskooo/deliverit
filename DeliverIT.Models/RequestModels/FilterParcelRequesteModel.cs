﻿namespace DeliverIT.Models.RequestModels
{
    public class FilterParcelRequesteModel
    {
#nullable enable
        public int? Weight { get; set; }
        public long? CustomerId { get; set; }
        public int? WarehouseId { get; set; }
        public string? CategoryName { get; set; }
        public bool? OrderByWeight { get; set; }
        public bool? OrderByArrivalDate { get; set; }
        public bool? OrderDescending { get; set; }
    }
}