﻿namespace DeliverIT.Models.RequestModels
{
    public class UpdateParcelRequestModel
    {
        public long Id { get; set; }
        public int Weight { get; set; }
        public bool? DeliveryToAddress { get; set; }
        public long? CustomerId { get; set; }
        public int? WarehouseId { get; set; }
        public long? CategoryId { get; set; }
        public long? ShipmentId { get; set; }
    }
}