﻿using System;

namespace DeliverIT.Models.RequestModels
{
    public class CreateShipmentRequestModel
    {
        public int OriginWarehouse { get; set; }
        public int DestinationWarehouse { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
    }
}