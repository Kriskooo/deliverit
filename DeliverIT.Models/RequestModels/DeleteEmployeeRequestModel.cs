﻿namespace DeliverIT.Models.RequestModels
{
    public class DeleteEmployeeRequestModel
    {
        public long Id { get; set; }
    }
}