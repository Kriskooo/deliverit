﻿namespace DeliverIT.Models.RequestModels
{
    public class DeleteParcelRequestModel
    {
        public long Id { get; set; }
    }
}