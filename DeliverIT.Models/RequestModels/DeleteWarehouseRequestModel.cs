﻿namespace DeliverIT.Models.RequestModels
{
    public class DeleteWarehouseRequestModel
    {
        public long Id { get; set; }
    }
}