﻿namespace DeliverIT.Models.RequestModels
{
    public class GetShipmentsByCustomerRequestModel : GetPaginatedRequestModel
    {
        public long CustomerId { get; set; }
    }
}