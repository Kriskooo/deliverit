﻿namespace DeliverIT.Models.RequestModels
{
    public class UpdateWarehouseRequestModel
    {
        public long Id { get; set; }
        public int AddressId { get; set; }
    }
}