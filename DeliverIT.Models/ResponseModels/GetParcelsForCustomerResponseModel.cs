﻿using DeliverIT.Models.EntityModels;
using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetParcelsForCustomerResponseModel : GetPaginatedResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public IEnumerable<GetParcelResponseModel> Parcels { get; set; }
    }
}
