﻿namespace DeliverIT.Models.ResponseModels
{
    public class SearchCustomerResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
