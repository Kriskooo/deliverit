﻿namespace DeliverIT.Models.ResponseModels
{
    public class Response
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
