﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.ResponseModels
{
    public class TestRequstResponseModel
    {
        public int TotalPages { get; set; }
        public ResponseData Data { get; set; }
    }
}
