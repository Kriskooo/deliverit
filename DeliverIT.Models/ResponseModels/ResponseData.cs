﻿using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class ResponseData
    {
       public List<Response> Responses { get; set; }
    }
}
