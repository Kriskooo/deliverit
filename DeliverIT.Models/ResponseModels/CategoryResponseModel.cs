﻿namespace DeliverIT.Models.ResponseModels
{
    public class CategoryResponseModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
