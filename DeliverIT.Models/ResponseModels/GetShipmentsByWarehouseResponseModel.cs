﻿using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetShipmentsByWarehouseResponseModel : GetPaginatedResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public IEnumerable<GetShipmentsResponseModel> Shipments { get; set; }
    }
}
