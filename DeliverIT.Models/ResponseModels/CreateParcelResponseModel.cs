﻿namespace DeliverIT.Models.ResponseModels
{
    public class CreateParcelResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
