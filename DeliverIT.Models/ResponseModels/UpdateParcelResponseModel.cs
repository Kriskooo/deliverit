﻿namespace DeliverIT.Models.ResponseModels
{
    public class UpdateParcelResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
