﻿namespace DeliverIT.Models.ResponseModels
{
    public class UpdateCustomerResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
