﻿using System;

namespace DeliverIT.Models.ResponseModels
{
    public class GetShipmentsResponseModel
    {
        public long Id { get; set; }
        public int OriginWarehouse { get; set; }
        public int DestinationWarehouse { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string StatusName { get; set; }
    }
}
