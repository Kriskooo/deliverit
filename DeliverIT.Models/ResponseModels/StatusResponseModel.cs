﻿namespace DeliverIT.Models.ResponseModels
{
    public class StatusResponseModel
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
