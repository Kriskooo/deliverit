﻿namespace DeliverIT.Models.ResponseModels
{
    public class UpdateShipmentResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
