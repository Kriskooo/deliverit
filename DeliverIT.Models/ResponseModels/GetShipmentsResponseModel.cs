﻿using System;
using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetShipmentResponseModel
    {
        public long Id { get; set; }
        public int OriginWarehouseId { get; set; }
        public int DestinationWarehouseId { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }

        public byte StatusId { get; set; }
        public IEnumerable<StatusResponseModel> PossibleStatuses { get; set; }
    }
}
