﻿namespace DeliverIT.Models.ResponseModels
{
    public class GetPaginatedResponseModel
    {
        public int TotalCount { get; set; }
        public int CurrentPage { get; set; }
        public bool IsLastPage { get; set; }
    }
}
