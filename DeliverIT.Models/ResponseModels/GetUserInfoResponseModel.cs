﻿namespace DeliverIT.Models.ResponseModels
{
    public class GetUserInfoResponseModel
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserRoleName { get; set; }
    }
}
