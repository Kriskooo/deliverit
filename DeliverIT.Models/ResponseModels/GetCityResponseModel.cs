﻿using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetCityResponseModel
    {
        public List<string> Cities { get; set; }
    }
}
