﻿namespace DeliverIT.Models.ResponseModels
{
    public class DeleteWarehouseResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}

