﻿namespace DeliverIT.Models.ResponseModels
{
    public class DeleteShipmentResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
