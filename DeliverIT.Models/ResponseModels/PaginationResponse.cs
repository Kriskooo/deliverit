﻿namespace DeliverIT.Models.ResponseModels
{
    public class PaginationResponse
    {
        public int CurrentPage { get; set; }
        public bool IsLastPage { get; set; }
    }
}
