﻿namespace DeliverIT.Models.ResponseModels
{
    public class GetLoggedUserInfoResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string UserRoleName { get; set; }

        public long Id { get; set; }

    }
}
