﻿namespace DeliverIT.Models.ResponseModels
{
    public class DeleteCustomerResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
