﻿using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CountryController : Controller
    {
        private readonly ICountryService countryService;

        public CountryController(ICountryService service)
        {
            this.countryService = service;
        }

        [HttpGet]
        public async Task<IActionResult> All()
        {
            var response = await countryService.GetCountries();
            return Json(response);
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetCountries()
        {
            var response = await countryService.GetCountries();
            return View(response);
        }
    }
}