﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WarehouseController : Controller
    {
        private readonly IWarehouseService warehouseService;

        public WarehouseController(IWarehouseService warehouseService)
        {
            this.warehouseService = warehouseService;
        }

        [HttpGet]
        public async Task<IActionResult> GetWarehouseByCity([FromQuery] GetWarehouseRequestModelByCity requestModel)
        {
            var response = await warehouseService.GetWarehouseByCityAsync(requestModel);
            return Json(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetWarehouseByCountry([FromQuery] GetWarehouseRequestModelByCountry requestModel)
        {
            var response = await warehouseService.GetWarehouseByCountryAsync(requestModel);
            return Json(response);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([FromBody] CreateWarehouseRequestModel requestModel)
        {
            var result = await warehouseService.CreateWarehouseAsync(requestModel);
            return Ok(result);
        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update([FromBody] UpdateWarehouseRequestModel requestModel)
        {
            var result = await warehouseService.UpdateWarehouseAsync(requestModel);
            return Ok(result);
        }

        [HttpDelete]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete([FromQuery] DeleteWarehouseRequestModel requestModel)
        {
            var result = await warehouseService.DeleteWarehouseAsync(requestModel);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var response = await warehouseService.TotalWarehousesAsync();
            return Json(response);
        }

    }
}
