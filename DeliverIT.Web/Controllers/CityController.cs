﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CityController : Controller
    {
        private readonly ICityService cityService;

        public CityController(ICityService service)
        {
            this.cityService = service;
        }

        [HttpGet]
        public async Task<IActionResult> All()
        {
            var response = await cityService.GetCities();
            return Json(response);
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetCities()
        {
            var response = await cityService.GetCities();
            return View(response); 
        }
    }
}

