﻿let currentPageNumber = 1;
const PageSize = 10;

$("#container-table").on("click", "#prev-button", function (event) {
  getPreviousPageData();
});

$("#container-table").on("click", "#next-button", function (event) {
  getNextPageData();
});

getData(currentPageNumber);

function getPreviousPageData() {
  if (currentPageNumber > 0) {
    currentPageNumber--;
    getData(currentPageNumber);
  }
}

function getNextPageData() {
  currentPageNumber++;
  getData(currentPageNumber);
}

function getData(newPageNumber) {
  let skipAmount = (newPageNumber * PageSize) - PageSize;

  let data = JSON.stringify({
    skip: skipAmount,
    take: PageSize
  });

  $.ajax({
    url: "/api/Shipment/GetShipmentsMvc",
    type: "POST",
    data: data,
    contentType: "application/json; charset=utf-8",
    success: function (response) {
        $("#container-table").empty();
        $("#container-table").append(response);
    },
    error: function () {

    }
  });
}