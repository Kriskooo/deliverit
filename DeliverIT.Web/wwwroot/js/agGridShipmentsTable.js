﻿let currentPageNumber = 1;
const PageSize = 10;

const columnDefs = [
    { headerName: 'Id', field: 'id' },
    { headerName: 'Status Name', field: 'statusName', resizable: true },
    { headerName: 'Arrival Time', field: 'arrivalTime', resizable: true },
    { headerName: 'Departure Time', field: 'departureTime', resizable: true },
    { headerName: 'Destination Warehouse', field: 'destinationWarehouse', resizable: true },
    { headerName: 'Origin Warehouse', field: 'originWarehouse', resizable: true },
    {
        headerName: "Edit Action",
        field: "button",
        cellRenderer: function (params) {
            return `<a type="button" class="edit-parcel-button custom-button-link btn btn-primary" href="/api/Shipment/EditShipment?id=${params.data.id}">Edit</a>`;
        }
    },
    {
        headerName: "Delete Action",
        field: "button",
        cellRenderer: function (params) {
            return `<button class="delete-shipment-button btn btn-danger" row-index=${params.rowIndex} data-id=${params.data.id} href="#">Delete</button>`;            
        }
    }
];

const gridOptions = {
    columnDefs: columnDefs,
    rowData: []
};

const eGridDiv = document.querySelector('#myGrid');
new agGrid.Grid(eGridDiv, gridOptions);
gridOptions.api.sizeColumnsToFit();

getData(currentPageNumber);

function drawPagination(totalElements, currentPage) {

    let numberOfPages = Math.floor(totalElements / PageSize) + 1;

    let $prevButton = $("#prev-button");
    let $nextButton = $("#next-button")

    if (currentPage == 1) {
        $prevButton.prop("disabled", true);
        if (numberOfPages > 1) {
            $nextButton.prop("disabled", false);
        }
    }
    else if (currentPage == numberOfPages) {
        $nextButton.prop("disabled", true);
        if (numberOfPages > 1) {
            $prevButton.prop("disabled", false);
        }
    }
    else if (currentPage < numberOfPages) {
        $prevButton.prop("disabled", false);
        $nextButton.prop("disabled", false);
    }

    $("#page-counter").text("Page: " + currentPage);
}

$("#myGrid").on("click", ".delete-shipment-button", function (event) {
    let dsadada = $(event);

    let shipmentId = parseInt((($(event)[0]).currentTarget).getAttribute("data-id"));

    let dataToSend = JSON.stringify({
        id: shipmentId
    });

    $.ajax({
        url: "/api/Shipment/Delete",
        type: "DELETE",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {

            getData(currentPageNumber);
            
        },
        error: function () {
            
        }
    });
});

$("#prev-button").click(function (event) {
    getPreviousPageData();
});

$("#next-button").click(function (event) {
    getNextPageData();
});

function getPreviousPageData() {
    if (currentPageNumber > 0) {
        currentPageNumber--;
        getData(currentPageNumber);
    }
}

function getNextPageData() {    
    currentPageNumber++;
    getData(currentPageNumber);
}

function getData(newPageNumber) {
    let skip = (newPageNumber * PageSize) - PageSize;

    let dataToSend = JSON.stringify({
        take: PageSize,
        skip: skip
    });

    $.ajax({
        url: "/api/Shipment/GetShipments",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            gridOptions.api.setRowData(response.data);
            drawPagination(response.totalCount, response.currentPage)
        },
        error: function (response) {
          
        }
    });

}
