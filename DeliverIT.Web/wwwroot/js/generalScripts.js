﻿$("#login-form").submit(function (event) {
    event.preventDefault();
    var $inputs = $('#login-form :input.form-control');
    let username = $inputs[0].value;
    let password = $inputs[1].value;

    if (!username || username.length < 1 || !password || password.length < 1) {
        Swal.fire({
            icon: 'error',
            title: "You need to input longer credentials!",
            showConfirmButton: false,
            timer: 2000
        })
    }

    let dataToSend = JSON.stringify({
        username: username,
        password: password
    });

    $.ajax({
        url: "/api/Account/Login",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            Swal.fire({
                icon: 'success',
                title: response,
                showConfirmButton: false,
                timer: 2000
            }).then(() => {
                location.reload();
            });

        },
        error: function (response) {
            Swal.fire({
                icon: 'error',
                title: response.responseText,
                showConfirmButton: false,
                timer: 2000
            })
        }
    });

})

$("#register-button-custom").click(function () {
    $('#login-popup').modal('hide');
});

$("#register-form").submit(function (event) {
    event.preventDefault();
    var $inputs = $('#register-form :input.form-control');
    let username = $inputs[0].value;
    let password = $inputs[1].value;
    let firstName = $inputs[2].value;
    let lastName = $inputs[3].value;
    let email = $inputs[4].value;
    let countryName = $inputs[5].value;
    let cityName = $inputs[6].value;
    let address = $inputs[7].value;

    if (!username || username.length < 1 || !password || password.length < 1) {
        Swal.fire({
            icon: 'error',
            title: "You need to input longer credentials!",
            showConfirmButton: false,
            timer: 2000
        })
    }

    let dataToSend = JSON.stringify({
        username: username,
        password: password,
        firstName: firstName,
        lastName: lastName,
        email: email,
        countryName: countryName,
        cityName: cityName,
        address: address
    });

    $.ajax({
        url: "/api/Account/Register",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            Swal.fire({
                icon: 'success',
                title: response,
                showConfirmButton: false,
                timer: 2000
            }).then(() => {
                location.reload();
            });

        },
        error: function (response) {
            Swal.fire({
                icon: 'error',
                title: response.responseText,
                showConfirmButton: false,
                timer: 2000
            })
        }
    });

})
